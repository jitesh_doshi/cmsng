angular.module('cmsng', ['firebase'])
.value('storage_url', 'https://cmsng-local.firebaseio-demo.com/')
.controller('NodeCtrl', function($scope, storage_url, angularFire) {
  $scope.nodes = angularFire(storage_url + '/nodes', $scope, 'nodes');
  $scope.node = {};
  $scope.add = function() {
    $scope.nodes.push($scope.node);
    $scope.node = {}
  }
})
.controller('EditCtrl', function($scope) {
  $scope.node = $scope.$parent.n;
})
.directive('contenteditable', function() {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
      // view -> model
      elm.bind('blur', function() {
        scope.$apply(function() {
          ctrl.$setViewValue(elm.html());
        });
      });

      // model -> view
      ctrl.$render = function(value) {
        elm.html(value);
      };

      // load init value from DOM
      // ctrl.$setViewValue(elm.html());
    }
  };
});